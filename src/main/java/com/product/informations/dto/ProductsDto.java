package com.product.informations.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProductsDto {

     private Long id;
     private String name;
     private String brand;
     private Double price;
     private LocalDate create_date;
     private Integer count;
     private Boolean isActive;


}
