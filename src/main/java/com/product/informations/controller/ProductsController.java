package com.product.informations.controller;

import com.product.informations.dto.ProductsDto;
import com.product.informations.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductsController {

    private final ProductService productService;

    @GetMapping("/{id}")
    public ProductsDto getProductsById(@PathVariable Long id) {

        return productService.getProductsById(id);

    }

    @PostMapping
    public ProductsDto createProducts(@RequestBody ProductsDto productsDto) {
        return productService.createProduct(productsDto);

    }

    @PutMapping
    public ProductsDto updateProducts(@RequestBody ProductsDto dto) {

        return productService.updateProduct(dto);
    }

    @DeleteMapping("/{id}")
    void deleteProductsById(@PathVariable Long id) {

        productService.deleteProductById(id);
    }
}
