package com.product.informations.service;

import com.product.informations.dto.ProductsDto;

public interface ProductService {

   ProductsDto getProductsById(Long id);

   ProductsDto createProduct(ProductsDto productsDto);

   ProductsDto updateProduct(ProductsDto dto);

   void deleteProductById(Long id);

}
