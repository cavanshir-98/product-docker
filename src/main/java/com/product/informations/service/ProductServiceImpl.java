package com.product.informations.service;

import com.product.informations.dto.ProductsDto;
import com.product.informations.model.Product;
import com.product.informations.repository.ProductRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;
    private final ModelMapper modelMapper ;

    @Override
    public ProductsDto getProductsById(Long id) {
        Product byId = productRepo.getById(id);
        ProductsDto map = modelMapper.map(byId, ProductsDto.class);
        return map;
    }

    @Override
    public ProductsDto createProduct(ProductsDto productsDto) {
        Product save = modelMapper.map(productsDto, Product.class);
        Product save1 = productRepo.save(save);
        ProductsDto map = modelMapper.map(save1, ProductsDto.class);
        return map;

    }

    @Override
    public ProductsDto updateProduct(ProductsDto dto) {

        Product byId = productRepo.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Product not found")));
        byId.setName(dto.getName());
        byId.setBrand(dto.getBrand());
        byId.setPrice(dto.getPrice());
        byId.setCount(dto.getCount());
        byId.setCreate_date(dto.getCreate_date());
        byId.setIsActive(dto.getIsActive());
        Product save = productRepo.save(byId);
        return modelMapper.map(save, ProductsDto.class);

    }

    @Override
    public void deleteProductById(Long id) {
      productRepo.deleteById(id);

    }
}
